import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../services/items.service';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Item } from '../classes/item';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.scss']
})
export class NewItemComponent implements OnInit {

  snackBarRef: MatSnackBarRef<SimpleSnackBar>;
  item = new Item();
  constructor(private itemService:ItemsService, private snackBar:MatSnackBar, private router:Router) { }

  ngOnInit() {
  }

  createItem(){
    this.itemService.createItem(this.item)
    .then((response) => {
      this.itemService.getItems();
      this.snackBarRef = this.snackBar.open("Erfolgreich angelegt", "Zurück zum Lager", {duration: 3000});
      this.snackBarRef.onAction().subscribe(() => {
        this.router.navigateByUrl('/lager');
      })
    })
    .catch((err) =>{
      console.error(err);
    })
  }

}
