import { Component, OnInit } from '@angular/core';
import { List } from '../classes/list';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ListService } from '../services/list.service';

@Component({
  selector: 'app-new-list',
  templateUrl: './new-list.component.html',
  styleUrls: ['./new-list.component.scss']
})
export class NewListComponent implements OnInit {

  snackBarRef: MatSnackBarRef<SimpleSnackBar>;
  list = new List;

  constructor(private listService: ListService, private snackBar:MatSnackBar, private router:Router) { }

  ngOnInit() {
  }

  public createList() {
    this.listService.createList(this.list)
    .then(() => {
      this.snackBar.open('Erfolgreich erstellt', null, {duration: 3000});
    })
    .catch((err) => {
      console.error(err);
    })
  }

}
