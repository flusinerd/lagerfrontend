export class Cable {
  id: number
  name: string
  qrcode: string
  amount: number
  amountTotal: number
  createdAt?: Date
  updatedAt?: Date
}
