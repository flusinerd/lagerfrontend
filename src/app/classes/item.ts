export class Item {
  id: number
  name: string
  qrcode: string
  comment?: string
  outside: boolean = false
  isChest: boolean = false
  createdAt?: Date
  updatedAt?: Date
}
