import { Item } from './item';

export class List {
  id: number
  createdAt?: Date
  updatedAt?: Date
  items: Item[]
  back: boolean = false
  name: string
  cables: any;
}
