import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LagerComponent } from './lager/lager.component';
import { ItemEditorComponent } from './item-editor/item-editor.component';
import { NewItemComponent } from './new-item/new-item.component';
import { NewCableComponent } from './new-cable/new-cable.component';
import { CableEditorComponent } from './cable-editor/cable-editor.component';
import { ListComponent } from './list/list.component';
import { NewListComponent } from './new-list/new-list.component';
import { AllListsComponent } from './all-lists/all-lists.component';


const routes: Routes = [
  { path: 'lager', component: LagerComponent},
  { path: 'items/new', component: NewItemComponent},
  { path: 'items/:id', component: ItemEditorComponent},
  { path: 'cables/new', component: NewCableComponent},
  { path: 'cables/:id', component: CableEditorComponent},
  { path: 'lists', component: ListComponent},
  { path: 'lists/new', component: NewListComponent},
  { path: 'lists/:id', component: ListComponent},
  { path: '**', redirectTo: 'lager'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
