import { Component, OnInit } from '@angular/core';
import { CableService } from '../services/cable.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { Cable } from '../classes/cable';

@Component({
  selector: 'app-cable-editor',
  templateUrl: './cable-editor.component.html',
  styleUrls: ['./cable-editor.component.scss']
})
export class CableEditorComponent implements OnInit {
  snackBarRef: MatSnackBarRef<SimpleSnackBar>;
  cable = new Cable();

  constructor(private cableService:CableService, private router:Router, private snackBar: MatSnackBar, private route:ActivatedRoute) { }

  ngOnInit() {
    this.cableService.getCable(+this.route.snapshot.paramMap.get("id"))
    .then((cable:Cable) =>{
      this.cable = cable;
    })
    .catch((err) => {
      console.error(err);
    })
  }

  saveCable(){
    this.cableService.saveCable(this.cable)
    .then(() => {
      this.cableService.getCables();
      this.snackBarRef = this.snackBar.open("Erfolgreich gespeichert", "Zurück zum Lager", {duration: 3000});
      this.snackBarRef.onAction().subscribe(() => {
        this.router.navigateByUrl('/lager');
      })
    })
    .catch((err) =>{
      console.error(err);
    })
  }

  deleteCable(){
    this.cableService.deleteCable(this.cable)
    .then(() => {
      this.snackBar.open("Erfolgreich gelöscht", null, {duration: 3000});
      this.router.navigateByUrl('/lager');
    })
    .catch((err) => {
      console.error(err);
    })
  }

}
