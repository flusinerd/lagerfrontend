import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CableEditorComponent } from './cable-editor.component';

describe('CableEditorComponent', () => {
  let component: CableEditorComponent;
  let fixture: ComponentFixture<CableEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CableEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CableEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
