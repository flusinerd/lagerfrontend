import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MatTableModule, MatSortModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatSlideToggleModule, MatProgressSpinnerModule, MatSnackBarModule, MatSelectModule } from '@angular/material'  
import {MatPaginatorModule} from '@angular/material/paginator';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LagerComponent } from './lager/lager.component';
import { ItemEditorComponent } from './item-editor/item-editor.component';
import { NewItemComponent } from './new-item/new-item.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NewCableComponent } from './new-cable/new-cable.component';
import { CableEditorComponent } from './cable-editor/cable-editor.component';
import { ListComponent } from './list/list.component';
import { NewListComponent } from './new-list/new-list.component';
import { AllListsComponent } from './all-lists/all-lists.component';



@NgModule({
  declarations: [
    AppComponent,
    LagerComponent,
    ItemEditorComponent,
    NewItemComponent,
    NewCableComponent,
    CableEditorComponent,
    ListComponent,
    NewListComponent,
    AllListsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatInputModule,
    MatSlideToggleModule,
    HttpClientModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatSelectModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
