import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Item } from '../classes/item';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ItemsService } from '../services/items.service';
import { CableService } from '../services/cable.service';

@Component({
  selector: 'app-lager',
  templateUrl: './lager.component.html',
  styleUrls: ['./lager.component.scss']
})
export class LagerComponent implements AfterViewInit {

  items: MatTableDataSource<any> = new MatTableDataSource([]);

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  columnsToDisplay = ['name', 'qrcode', 'amountTotal', 'outside', 'actions'];
  constructor(private itemService:ItemsService, private cableService:CableService) { 
    this.itemService.getItems();
    this.cableService.getCables();
    this.itemService.itemsSubject.subscribe((data) => {this.updateLager()});
    this.cableService.cablesSubject.subscribe((data) => {this.updateLager()})
  }

  private updateLager(){
    let promises = [];
    promises.push(this.itemService.getItemsPromise());
    promises.push(this.cableService.getCablesPromise());

    Promise.all(promises)
    .then((results:Array<any>) => {
      this.items.data = results[0].concat(results[1]);
    })
  }

  ngAfterViewInit() {
    this.items.paginator = this.paginator;
    this.items.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.items.filter = filterValue;
  }

}
