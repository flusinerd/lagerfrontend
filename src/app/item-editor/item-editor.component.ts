import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../services/items.service';
import { ActivatedRoute, Router } from "@angular/router";
import { Item } from '../classes/item';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';

@Component({
  selector: 'app-item-editor',
  templateUrl: './item-editor.component.html',
  styleUrls: ['./item-editor.component.scss']
})
export class ItemEditorComponent implements OnInit {

  constructor(private itemService:ItemsService, private route:ActivatedRoute, private snackBar: MatSnackBar, private router:Router) { }

  item:Item;
  snackBarRef: MatSnackBarRef<SimpleSnackBar>;

  ngOnInit() {
    this.itemService.getItem(+this.route.snapshot.paramMap.get("id"))
    .then((item:Item) =>{
      this.item = item;
    })
    .catch((err) => {
      console.error(err);
    })
  }

  saveItem(){
    this.itemService.saveItem(this.item)
    .then(() => {
      this.itemService.getItems();
      this.snackBarRef = this.snackBar.open("Erfolgreich gespeichert", "Zurück zum Lager", {duration: 3000});
      this.snackBarRef.onAction().subscribe(() => {
        this.router.navigateByUrl('/lager');
      })
    })
    .catch((err) =>{
      console.error(err);
    })
  }

  deleteItem(){
    this.itemService.deleteItem(this.item)
    .then(() => {
      this.snackBar.open("Erfolgreich gelöscht", null, {duration: 3000});
      this.router.navigateByUrl('/lager');
    })
    .catch((err) => {
      console.error(err);
    })
  }

}
