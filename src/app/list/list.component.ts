import { Component, OnInit } from '@angular/core';
import { List } from '../classes/list';
import { ListService } from '../services/list.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Item } from '../classes/item';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  list: List;
  displayList: any[];
  lists: List[];


  columnsToDisplay = ['name', 'qrcode', 'amountTotal', 'outside', 'actions'];
  constructor(private listService: ListService, private route:ActivatedRoute, private router:Router, private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.listService.getLists();
    this.listService.listSubject.subscribe((lists:List[]) => this.lists = lists);
    this.route.paramMap.subscribe(map => {
      this.listService.getListById(map.get('id'))
      .then((list:List) => {
        this.list = list;
        list.cables.forEach(element => {
          element.name = element.cable.name;
          element.amount = element.cable.amount;
          element.qrcode = element.cable.qrcode;
          element.comment = element.cable.comment;
        });
        this.displayList = list.items.concat(list.cables);
      })
      .catch((err) => {
        console.error(err);
      })
    })
  }

  public deleteList(){
    this.listService.deleteList(this.list)
    .then(() => {
      this.router.navigateByUrl("/lists");
      this.snackBar.open("Erfolgreich gelöscht", null, {duration: 3000});
      this.listService.getLists();
    })
    .catch((err) => {
      console.error(err);
    })
  }

  public removeItemFromList(item: Item){
    // this.list.items = this.arrayRemove(this.list.items, item);
    this.listService.removeFromList(item, this.list)
    .then((list: List) => {
      console.log(list);
      this.list = list;
      this.snackBar.open("Aus Liste entfernt", null, {duration: 3000});
    })
    .catch((err) =>{
      console.error(err);
    });
  }

  private arrayRemove(arr:Item[], value:Item){
    return arr.filter(function (element) {
      return element.id != value.id;
    });
  }

  

}
