import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Item } from '../classes/item';


@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  itemsSubject = new BehaviorSubject<Item[]>([]);

  constructor(private http: HttpClient) { }

  /**
   * getItems
   */
  public getItems() {
    this.http.get(`${environment.apiUrl}/items`).toPromise()
      .then((data: Item[]) => {
        this.itemsSubject.next(data);
      })
      .catch((err) => {
        console.error(err);
      })
  }
  
  public getItemsPromise(): Promise<any>{
    return this.http.get(`${environment.apiUrl}/items`).toPromise();
  }

  public getItem (id: number): Promise<Object>{
    return this.http.get(`${environment.apiUrl}/items/${id}`).toPromise();
  }

  public getItemByQr (qrcode: string): Promise<Object>{
    return this.http.get(`${environment.apiUrl}/items/qrcode/${qrcode}`).toPromise();
  }

  public saveItem (item: Item): Promise<Object>{
    return this.http.put(`${environment.apiUrl}/items/${item.id}`, item).toPromise();
  }

  public createItem (item: Item): Promise<Object>{
    return this.http.post(`${environment.apiUrl}/items`, item).toPromise();
  }

  public deleteItem (item: Item): Promise<Object>{
    return this.http.delete(`${environment.apiUrl}/items/${item.id}`).toPromise();
  }
}
