import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Cable } from '../classes/cable';

@Injectable({
  providedIn: 'root'
})
export class CableService {

  public cablesSubject = new BehaviorSubject<Cable[]>([])
  constructor(private http:HttpClient) { }

  public getCables() {
    this.http.get(`${environment.apiUrl}/cables`).toPromise()
      .then((data: Cable[]) => {
        this.cablesSubject.next(data);
      })
      .catch((err) => {
        console.error(err);
      })
  }

  public getCablesPromise(): Promise<any> {
    return this.http.get(`${environment.apiUrl}/cables`).toPromise()
  }

  public getCable (id: number): Promise<Object>{
    return this.http.get(`${environment.apiUrl}/cables/${id}`).toPromise();
  }

  public getItemByQr (qrcode: string): Promise<Object>{
    return this.http.get(`${environment.apiUrl}/cables/qrcode/${qrcode}`).toPromise();
  }

  public saveCable (cable: Cable): Promise<Object>{
    return this.http.put(`${environment.apiUrl}/cables/${cable.id}`, cable).toPromise();
  }

  public createCable (cable: Cable): Promise<Object>{
    return this.http.post(`${environment.apiUrl}/cables`, cable).toPromise();
  }

  public deleteCable (cable: Cable): Promise<Object>{
    return this.http.delete(`${environment.apiUrl}/cavles/${cable.id}`).toPromise();
  }
}
