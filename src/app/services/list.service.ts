import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { List } from '../classes/list';
import { Item } from '../classes/item';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  listSubject = new BehaviorSubject<List[]>([]);
  currentList: List;
  constructor(private http:HttpClient) { }

  public getLists(){
    this.http.get(`${environment.apiUrl}/lists`).toPromise()
    .then((lists:List[]) => {
      this.listSubject.next(lists);
    });
  }

  public getList(list: List): Promise<Object>{
    return this.http.get(`${environment.apiUrl}/lists/${list.id}`).toPromise()
  }

  public deleteList(list: List): Promise<Object>{
    return this.http.delete(`${environment.apiUrl}/lists/${list.id}`).toPromise();
  }

  public updateList(list: List): Promise<Object>{
    return this.http.put(`${environment.apiUrl}/lists/${list.id}`, list).toPromise();
  }

  public createList(list: List): Promise<Object>{
    return this.http.post(`${environment.apiUrl}/lists`, list).toPromise();
  }

  public getListById(id: string): Promise<Object>{
    return this.http.get(`${environment.apiUrl}/lists/${id}`).toPromise();
  }

  public removeFromList(item: Item, list: List): Promise<Object>{
    return this.http.post(`${environment.apiUrl}/lists/${list.id}/remove`, {item: item}).toPromise();
  }

}
