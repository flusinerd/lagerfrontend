import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { CableService } from '../services/cable.service';
import { Cable } from '../classes/cable';

@Component({
  selector: 'app-new-cable',
  templateUrl: './new-cable.component.html',
  styleUrls: ['./new-cable.component.scss']
})
export class NewCableComponent implements OnInit {

  snackBarRef: MatSnackBarRef<SimpleSnackBar>;
  cable = new Cable();

  constructor(private cableService:CableService, private router:Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  createCable(){
    this.cableService.createCable(this.cable)
    .then((response) => {
      this.cableService.getCables();
      this.snackBarRef = this.snackBar.open("Erfolgreich angelegt", "Zurück zum Lager", {duration: 3000});
      this.snackBarRef.onAction().subscribe(() => {
        this.router.navigateByUrl('/lager');
      })
    })
    .catch((err) =>{
      console.error(err);
    })
  }

}
