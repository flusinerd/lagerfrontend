import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCableComponent } from './new-cable.component';

describe('NewCableComponent', () => {
  let component: NewCableComponent;
  let fixture: ComponentFixture<NewCableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
